
public class Rekursiv {
	// detta är en metod som räknar ner med 1
	public void rekursiv(int räkna) {
		System.out.println(räkna + "rekursivfunktion");
		if (räkna <= 0) {
			System.out.println("klart");
		} else {
			rekursiv(räkna - 1);
		}
	}

	public static void main(String[] args) {
		// Här instansierar jag klassan och anropar min metod
		Rekursiv r = new Rekursiv();
		r.rekursiv(10);

	}

}
